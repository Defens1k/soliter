import screen
import cardDeck
import time
import collections



columnsCount = 7
queueInitSize = 24

def deepKey(card):
  return card['deep']

class Queue:

    def __init__(self, deck):
        self.deck = deck
        self.left = collections.deque()
        self.right = collections.deque()
        self.moveNumber = 0
        for i in range(queueInitSize):
            card = deck.getClosedCard()
            self.right.append(card)
    def size(self):
        return len(self.left) + len(self.right)


    def nextCard(self, moveNumber):
        self.prevMoveNumber = self.moveNumber
        self.moveNumber = moveNumber
        if self.size() == 1:
            if len(self.left) == 0:
                screen.nextQueue()
                card = self.right.popleft()
                if card['isClosed'] == True:
                    card = self.deck.getQueueCard()
                self.left.append(card)
            return self.left[0]
        if len(self.left) == 0:
            screen.nextQueue()
            card = self.right.popleft()
            if card['isClosed'] == True:
                card = self.deck.getQueueCard()
            self.left.append(card)
            return card
        if self.prevMoveNumber != self.moveNumber - 1:
            return self.left[-1]
        if len(self.right) == 0:
            screen.nextQueue()
            for i in range(len(self.left)):
                card = self.left.pop()
                self.right.appendleft(card)
            screen.nextQueue()
            card = self.right.popleft()
            self.left.append(card)
            return card
        screen.nextQueue()
        card = self.right.popleft()
        if card['isClosed'] == True:
            card = self.deck.getQueueCard()
        self.left.append(card)
        return card
  

    def deleteCurrent(self):
        self.moveNumber = 0
        self.left.pop()
    def isEmpty(self):
        return self.size() == 0
            

class Exit:
    def __init__(self):
        self.exits = dict()
        for suit in cardDeck.suitToColor.keys():
            self.exits[suit] = list()
            for num in cardDeck.cards:
                self.exits[suit].append(num)
    def addCard(self, suit):
        self.exits[suit].pop()
    def getAvalible(self, suit):
        return self.exits[suit][-1]
    def isNotFull(self):
        for suit in self.exits.keys():
            if len(self.exits[suit]) > 0:
                return True
        return False

class Table:
    def __init__(self):
        self.columns = list()
        self.deck = cardDeck.CardDeck()
        self.exit = Exit()
        for columnIndex in range(columnsCount):
            column = list()
            for i in range(columnIndex):
                column.append(self.deck.getClosedCard())
            column.append(self.deck.getOpennedCard(columnIndex, columnIndex))
            self.columns.append(column)
        print(self.columns)
        self.queue = Queue(self.deck)
        return

    def getTopCards(self):
        topCards = list()
        for columnNum in range(len(self.columns)):
            column = self.columns[columnNum]
            if len(column) == 0:
                continue
            topCard = column[-1]
            topCards.append({'card': topCard, 'column': columnNum, 'deep': len(column) - 1})
        # print("TopCards: ", topCards)
        return topCards

    def getDeepCards(self):
        deepCards = list()
        for columnNum in range(len(self.columns)):
            column = self.columns[columnNum]
            for deep in range(len(column)):
                if column[deep]['isClosed'] == False:
                    deepCards.append({'card': column[deep], 'column': columnNum, 'deep': deep})
                    break
        # print("DeepCards: ", deepCards)
        deepCards.sort(reverse = True, key = deepKey)
        return deepCards

    def hasEmptyColumns(self):
        for i in range(len(self.columns)):
            if len(self.columns[i]) == 0:
                return True, i
        return False, -1
    
    def getAvalibleKing(self):
        kingCandidates = list()
        for col in range(len(self.columns)):
            if len(self.columns[col]) == 0:
                continue
            for deep in range(1, len(self.columns[col])):
                card = self.columns[col][deep]
                if card['isClosed'] == False and card['num'] == "K":
                    kingCandidates.append({'column': col, 'deep': deep, 'card': card})
        if len(kingCandidates) == 0:
            return False, -1
        kingCandidates.sort(reverse = True, key = deepKey)
        return True, kingCandidates[0]

    def move(self, moveNumber):
        time.sleep(0.1)
        topCards = self.getTopCards()
        for topCard in topCards:
            if self.exit.getAvalible(topCard['card']['suit']) == topCard['card']['num']:
                self.exit.addCard(topCard['card']['suit'])
                screen.moveCardToExit(topCard['column'], topCard['deep'])
                self.columns[topCard['column']].pop()
                print("move to exit", topCard['column'], topCard['card'])
                if len(self.columns[topCard['column']]) == 0:
                    return
                if self.columns[topCard['column']][-1]['isClosed'] == False:
                    return
                self.columns[topCard['column']][-1] = self.deck.getOpennedCard(topCard['column'], topCard['deep'] - 1)
                return
        deepCards = self.getDeepCards()
        for deepCard in deepCards:
            for topCard in topCards:
                if cardDeck.isMatch(deepCard['card'], topCard['card']):
                    print("move from deep to top", deepCard['column'], deepCard['deep'], deepCard['card'], topCard['column'], topCard['deep'], topCard['card'])
                    screen.moveCard(deepCard['column'], deepCard['deep'], topCard['column'], topCard['deep'])
                    tmpDeck = list()
                    for i in range(deepCard['deep'], len(self.columns[deepCard['column']])):
                        tmpDeck.append(self.columns[deepCard['column']].pop())
                    tmpDeck.reverse()
                    for card in tmpDeck:
                        self.columns[topCard['column']].append(card)
                    if len(self.columns[deepCard['column']]) == 0:
                        return
                    self.columns[deepCard['column']][-1] = self.deck.getOpennedCard(deepCard['column'], deepCard['deep'] - 1)
                    return
        hasEmpty, emptyColumn = self.hasEmptyColumns()
        if hasEmpty:
            hasKing, kingCard = self.getAvalibleKing()
            if hasKing:
                print("move king to empty", kingCard['column'], kingCard['deep'], emptyColumn, kingCard['card'])
                screen.moveCard(kingCard['column'], kingCard['deep'], emptyColumn, 0)
                tmpDeck = list()
                for i in range(kingCard['deep'], len(self.columns[kingCard['column']])):
                    tmpDeck.append(self.columns[kingCard['column']].pop())
                tmpDeck.reverse()
                for card in tmpDeck:
                    self.columns[emptyColumn].append(card)
                if len(self.columns[kingCard['column']]) == 0:
                    return
                self.columns[kingCard['column']][-1] = self.deck.getOpennedCard(kingCard['column'], kingCard['deep'] - 1)
                return
        if self.queue.isEmpty():
            print("Warning: queue is empty")
            return
        queueCard = self.queue.nextCard(moveNumber)
        if self.exit.getAvalible(queueCard['suit']) == queueCard['num']:
            print("move to exit from queue", queueCard['suit'], queueCard['num'])
            self.exit.addCard(queueCard['suit'])
            screen.moveQueueCardToExit()
            self.queue.deleteCurrent()
            return
        for topCard in topCards:
            if cardDeck.isMatch(queueCard, topCard['card']):
                print("move from queue to top", topCard['column'], topCard['deep'], topCard['card'], queueCard)
                screen.moveQueueCardToTable(topCard['column'], topCard['deep'])
                self.columns[topCard['column']].append(queueCard)
                self.queue.deleteCurrent()
                return
        if hasEmpty and queueCard['num'] == "K":
            print("move king to empty from queue", emptyColumn)
            screen.moveQueueCardToTable(emptyColumn, 0)
            self.columns[emptyColumn].append(queueCard)
            self.queue.deleteCurrent()
            return

        print("Error: no move found")
            
                  
    def run(self):
        moveNumber = 1
        while self.exit.isNotFull():
            self.move(moveNumber)
            moveNumber += 1
        print("Game over", moveNumber)
