import os
import pyautogui
import time
from multiprocessing.pool import ThreadPool
from termcolor import colored

homedir = os.path.expanduser('~')
projectDir = homedir + '/soliter/'
screenFolder = projectDir + 'screens/'
imagesDir = projectDir + 'images/'
os.system('rm -rf ' + screenFolder)
os.system('mkdir ' + screenFolder)

cardWidth = 77
cardHeight = 27
leftBorder = 697
topBorder = 345
tableDistance = 152
cardScreenWindowSize = 25
cardBorder = 4

tableTopBorder = topBorder + tableDistance
width = cardWidth * 7
height = width
rightBorder = leftBorder + width

myScreenshot = pyautogui.screenshot(region=[leftBorder, topBorder, width, height])
myScreenshot.save(screenFolder + 'screen.png')

myScreenshot = pyautogui.screenshot(region=[leftBorder, tableTopBorder, width, height])
myScreenshot.save(screenFolder + 'table.png')

cards = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
suitToColor = {"H": "r", "D": "r", "P": "b", "C": "b"}

cardsConfidence = {
    "2": 0.95,
    "3": 0.95,
    "4": 0.95,
    "5": 0.95,
    "6": 0.97,
    "7": 0.95,
    "8": 0.97,
    "9": 0.95,
    "10": 0.92,
    "J": 0.95,
    "Q": 0.95,
    "K": 0.95,
    "A": 0.95
}

suitConfidence = {
    "H": 0.90,
    "D": 0.90,
    "P": 0.946,
    "C": 0.946
}


iter = 0
def getIter():
    global iter 
    iter += 1
    return str(iter)

def findNum(color, num):
    try:
        imgPath = imagesDir + num + color +'.png'
        result = pyautogui.locate(
                                needleImage = imgPath,
                                haystackImage = screenFolder + 'num.png',
                                confidence = cardsConfidence[num],
                                limit = 1,
                                grayscale=True)
        result = list(result)
        if len(result) > 0:
            print(result[0])
            return num
    except Exception as e:
        print(e, end = ' ')
    return ''


def getNum(color, avalible):
    pool = ThreadPool(processes=12)
    results = list()
    returned = list()
    for num in avalible:
        async_result = pool.apply_async(findNum, (color, num))
        results.append(async_result)
    for result in results:
        num = result.get()
        if num != '':
            returned.append(num)
    if len(returned) == 1:
        return returned[0]
    if len(returned) > 1:
        raise Exception('More than one num found')
    raise Exception('Num not found')


def moveCardToExit(col, deep):
    print('moveCardToExit')
    print(col, deep)
    x = leftBorder + cardWidth * col + cardBorder
    y = tableTopBorder + cardHeight * deep
    pyautogui.click(x, y)
    time.sleep(0.1)

def moveCard(fromCol, fromDeep, toCol, toDeep):
    xFrom = leftBorder + cardWidth * fromCol + cardBorder
    yFrom = tableTopBorder + cardHeight * fromDeep
    xTo = leftBorder + cardWidth * toCol + cardBorder
    yTo = tableTopBorder + cardHeight * toDeep
    pyautogui.moveTo(xFrom, yFrom, duration=0.01)
    pyautogui.dragTo(xTo, yTo, duration=0.2)

def moveQueueCardToTable(toCol, toDeep):
    xFrom = leftBorder + cardWidth * 5 + cardBorder
    yFrom = topBorder + cardBorder
    xTo = leftBorder + cardWidth * toCol + cardBorder
    yTo = tableTopBorder + cardHeight * toDeep
    pyautogui.moveTo(xFrom, yFrom, duration=0.01)
    pyautogui.dragTo(xTo, yTo, duration=0.2)


def findSuit(suit):
    try:
        imgPath = imagesDir + suit + '.png'
        conf = suitConfidence[suit]
        startTime = time.time()

        result = pyautogui.locate(
                                needleImage = imgPath,
                                haystackImage = screenFolder + 'suit.png',
                                confidence = conf,
                                limit = 1,
                                grayscale=True)
        endTime = time.time()
      
        print(colored('Time: ' + str(endTime - startTime),'green'))
        return suit
    except Exception as e: 
        print(e, end = ' ')
    return ''



def getSuit(x, y):
    pyautogui.screenshot(region=[x, y, cardScreenWindowSize, cardScreenWindowSize]).save(screenFolder + 'suit.png')
    pool = ThreadPool(processes=12)
    results = list()
    returned = list()
    for suit in suitToColor.keys():
        async_result = pool.apply_async(findSuit, (suit))
        results.append(async_result)
    for result in results:
        suit = result.get()
        if suit != '':
            returned.append(suit)
    if len(returned) == 1:
        return returned[0]
    if len(returned) > 1:
        raise Exception('More than one suit found')
    raise Exception('Suit not found')

def nextQueue():
    x = leftBorder + cardWidth * 6 + cardBorder * 2
    y = topBorder + cardBorder * 2
    pyautogui.click(x, y)
    time.sleep(0.7)

def numScreen(x, y):
    pyautogui.screenshot(region=[x, y, cardScreenWindowSize, cardScreenWindowSize]).save(screenFolder + 'num.png')
    return 0

def getQueueCard(avalible):
    time.sleep(0.001)
    xNum = leftBorder + cardWidth * 5 + cardBorder
    xSuit = leftBorder + cardWidth * 6 - cardBorder - 3 - cardScreenWindowSize
    y = topBorder + cardBorder

    pool = ThreadPool(processes=12)
    async_result = pool.apply_async(numScreen, (xNum, y))

    suit = getSuit(xSuit, y)

    async_result.get()
    num = getNum(suitToColor[suit], avalible[suit])
    return {'suit': suit, 'num': num}

def moveQueueCardToExit():
    x = leftBorder + cardWidth * 5 + cardBorder
    y = topBorder + cardBorder
    pyautogui.click(x, y)

def getCardFromTable(avalible, col, deep):
    time.sleep(0.2)
    xNum = leftBorder + cardWidth * col + cardBorder
    xSuit = leftBorder + cardWidth * (col + 1) - cardBorder - 2 - cardScreenWindowSize
    y = tableTopBorder + cardHeight * deep

    pool = ThreadPool(processes=12)
    async_result = pool.apply_async(numScreen, (xNum, y))

    suit = getSuit(xSuit, y)

    async_result.get()
    num = getNum(suitToColor[suit], avalible[suit])
    return {'suit': suit, 'num': num}

