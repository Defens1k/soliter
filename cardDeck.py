import screen
cards = ["K", "Q", "J", "10", "9", "8", "7", "6", "5", "4", "3", "2", "A"]
suitToColor = {"H": "r", "D": "r", "P": "b", "C": "b"}
matchNumMap = {
    "K": "Q",
    "Q": "J",
    "J": "10",
    "10": "9",
    "9": "8",
    "8": "7",
    "7": "6",
    "6": "5",
    "5": "4",
    "4": "3",
    "3": "2",
    "2": "A",
    "A": "undef"
}


class CardDeck:
    def __init__(self):
        self.avalibleCards = dict()
        for suit in suitToColor.keys():
            self.avalibleCards[suit] = list()
            for num in cards:
                self.avalibleCards[suit].append(num)

    def getClosedCard(self):
        return {'isClosed': True}
    def getOpennedCard(self, col, deep):
        card = screen.getCardFromTable(self.avalibleCards, col, deep)
        print("New card found: ", card['num'], card['suit'])
        card["isClosed"] = False
        self.avalibleCards[card['suit']].remove(card['num'])
        return card
    def getQueueCard(self):
        card = screen.getQueueCard(self.avalibleCards)
        print("New card found: ", card['num'], card['suit'])
        card["isClosed"] = False
        self.avalibleCards[card['suit']].remove(card['num'])
        return card



def isMatch(topCard, botCard):
    if suitToColor[topCard['suit']] == suitToColor[botCard['suit']]:
        return False
    if matchNumMap[botCard['num']] == topCard['num']:
        return True
    return False

